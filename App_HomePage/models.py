from django.db import models

# Create your models here.
from django import forms

kategori = [('Kepanitiaan', 'Kepanitiaan'),
            ('Academik', 'Akademik'),
            ('Organisasi', 'Organisasi')]

class model_Schedule(models.Model):
    activity_name = models.CharField(max_length=50)
    date = models.DateTimeField()
    location = models.CharField(max_length=100)
    category = models.CharField(max_length = 20, choices = kategori)

    def __str__ (self):
        return self.activity_name