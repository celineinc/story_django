from django import forms

#VISUALISASI SCHEDULE FORM

attrs ={
    'class' : 'form-control'
}

attrsDateTime={
    'class' : 'form-control',
    'type' : 'datetime-local'
}

class input_Schedule(forms.Form):
    activity_name = forms.CharField(label = "Activity's Name", required=True, max_length=50, empty_value="", widget = forms.TextInput(attrs= attrs))
    date = forms.DateTimeField(label = "Time and Date", required=True, input_formats= ['%Y-%m-%dT%H:%M'], widget = forms.DateTimeInput(attrs=attrsDateTime))
    location = forms.CharField(label = "Location", required=True, max_length=100, empty_value="", widget = forms.TextInput(attrs= attrs))
    category = forms.ChoiceField(choices = [('Kepanitiaan', 'Kepanitiaan'), ('Akademik', 'Akademik'), ('Organisasi', 'Organisasi')],
                                    label = "Category", required=True)