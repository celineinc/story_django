from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.home, name = "home"),
    path("profile", views.profile, name = "profile"),
    path("my_works", views.my_works, name = "my_works"),
    path("register", views.register, name = "register"),
    path("schedule", views.sch_form, name = "sch_form"),
    path("get_sch", views.get_form, name = "get_schedule"),
    path("table_sch", views.present_form, name = "table_sched"),
    path("clear_sch", views.clear_sch, name = "clear_schedule"),
]