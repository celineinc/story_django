from django.shortcuts import render, HttpResponseRedirect
from .forms import input_Schedule
from .models import model_Schedule

# Create your views here.
def home(request):
    context = {'home':'active'}
    return render (request, "home.html", context)

def profile(request):
    context = {'prof':'active'}
    return render (request, "profile.html",context)

def my_works(request):
    context = {'my_works':'active'}
    return render (request, "my_works.html", context)

def register(request):
    context = {'regist':'active'}
    return render (request, "register.html", context)

#Function to present our form when the tab menu 'add schedule' is clicked
def sch_form(request):
    form = input_Schedule()
    return render (request, 'sch_form.html', {'formulir' : form})

#Function to connect / enter submission from user to database
def get_form(request):
    form = input_Schedule(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {}
        response['activity_name'] = request.POST['activity_name'] if request.POST['activity_name'] != "" else "Anonymous"
        response['date'] = request.POST['date'] if request.POST['date'] != "" else "Anonymous"
        response['location'] = request.POST['location'] if request.POST['location'] != "" else "Anonymous"
        response['category'] = request.POST['category'] if request.POST['category'] != "" else "Anonymous"
        input_form = model_Schedule(activity_name=response['activity_name'], date=response['date'],
            location=response['location'], category=response['category'])
        input_form.save()
        html ='feedback_form.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('schedule')

#Function to present our activities after added
def present_form(request):
    sched = model_Schedule.objects.all()
    return render(request, 'activities.html', {'timetable' : sched})

#Function to clear all the activities
def clear_sch(request):
    sch_cleared = model_Schedule.objects.all().delete()
    return render(request, 'activities.html', {'timetable' : sch_cleared})
