# Generated by Django 2.1.1 on 2018-10-03 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='model_Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('activity_name', models.CharField(max_length=50)),
                ('date', models.DateTimeField()),
                ('location', models.CharField(max_length=100)),
                ('category', models.CharField(choices=[('Panit', 'Kepanitiaan'), ('Acad', 'Akademik'), ('Org', 'Organisasi')], max_length=20)),
            ],
        ),
    ]
